# Actors

## Admin

Admin has full access to all system features. Admins need eXistdb-specific 
know-how in case of unexpected behavior of the system.

## ShcAdmin

ShcAdmin is allowed to assign the editor role to application users.
ShcAdmins can approve or reject annotations suggested by other users.

The ShcAdmin role must explicitly be granted to at least one ShcUser to manage
the system and approve annotations.

## ShcUser

ShcUser is a person registered with the application. ShcUsers can create private or 
public annotations and suggest corrections to ShcEditors. ShcUsers can edit or delete
their own annoations.

## Reader

Any site visitor may browse through the texts and public annotations and may
register as a user.


