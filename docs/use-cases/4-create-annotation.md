# Use Case #1

## Goal

A user makes a text selection and creates an annotation for it. 

## Actor

user

## Precondition(s)

Optional:
what needs to be in place for this use case to happen 

A person must register itself with the application in order to access the system with
'create annotation' priviledge.

## Trigger event

The user clicks on a word or drags the mouse to select multiple word (range selection).

how is this use case triggered?

## Success condition

An annotation has been created and is stored for review with state 'proposed'.

short statement about 'what is the case' when this use case completed


## Success Scenario

step-by-step actions of the user 

1. user makes a text selection by clicking on a single word or by dragging 
the mouse. The application will automatically select all involved words of a selection 
completely.
1. a popup window appears displaying the content of the selection
1. user changes word(s) and confirms the change by either hitting enter or clicking 'ok'
1. The display will show the changed content within the text with a highlight.

## UI mockups

optional section to detail the user interface. May link to screenshot or graphical mockups

## Restrictions

Optional:
Any restrictions apply to this use case?

## Frequency of use

e.g. 'once a day', 'once a month', 'dozens of times an hour'. High frequency usually indicates a high priority for the implementation

## Questions

* we need to decide whether inline- or popup editors should be used where possible.

