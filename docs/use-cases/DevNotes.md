# Dev Notes: Early Modern Lab

## Overview

Northwestern created an application "Shakespeare His Contemporaries" which will serve as the main showcase for the annotation module. The application is already [live](http://shakespearehiscontemporaries.northwestern.edu/shc/) and will be further developed. It represents kind of the read-only view which we will extend into a collaborative website.

It should be clear that the annotation module must not be limited to the particular example of "Shakespeare His Contemporaries". Rather it should be possible to plug the annotation module into any application following the basic structure created by the TEI PM app generator.

Ideally most of the annotation module would come as a separate library package, providing general resources, XQuery modules and services to any application which wants to make use of it. For sure the target app will need to use those libraries and resources from within its HTML pages, but the goal should be to limit required changes to the minimum and make setup as easy as possible. Remember: the target audience are XML-savy scholars with very limited experience in web development.

## Setup

The source code for "Shakespeare His Contemporaries" as well as the source texts are available in gitlab:

http://admin.exist-db.org:9090/groups/the-early-modern-lab

Build the app with ant first, then upload the texts by logging into the "Browse" view (User: shcadmin, Password: admin).

Because the XML documents do explicitely wrap all whitespace into <w> elements, we'll need Leif-Jöran's whitespace fix for eXist! Without it, text display will never work properly as there's either too much or swallowed space! The fix has not yet been merged into develop, but I'll try to work it out with Leif.

## Development Workflow

I suggest to create a separate branch for the annotation module, so Northwestern can continue to work on master.

As for the workflow, we'll use a simplified gitflow using pull requests:

* when you start working on a feature or bug, create a new branch on the main repo (i.e. without forking it), prefixing it with either feature/ or bugfix/
* push your branch to gitlab (after rebasing) and open a merge request on gitlab

## First Steps

The goal for the next weeks should be to create the fundament for supporting the three edit actions: change, delete, insert. The edit actions mainly target the `<w>` elements and change their attributes.

1. Change the PM to output custom HTML elements for `<w>` instead of replacing them with spans
2. Include polymer for web components. I'm not sure how this could be done in a portable way. Ideally I think the webcomponents should live in their own library package, so they can be reused by multiple apps
3. Implement a simple webcomponent to edit the properties of any <w> element the user clicks on
4. Store edits as standoff annotations, which means we have to come up with a format for those annotations largely compatible with the [Web Annotation Data Model](http://www.w3.org/TR/annotation-model/)
5. Create concepts for the workflow: 
    * maintain a user session which collects all standoff annotations done by a user until he commits them for review
    * how will editors review proposed annotations?

*Open questions*:

I did not understand yet when a change would be recorded as `<orig>/<reg>`. Normally one would use `<reg>` to provide a regularized form for a non-standard form used in the transcribed text. However, our users will mainly fix defects in the transcription itself, e.g.

```XML
<w lemma="desire●" ana="#n1" type="unclear" xml:id="A15498-086540" facs="15-b-2880">desire●</w>
```

Marking this up with orig/reg would seem wrong.

>[magda] It is generally wrong, but for the sake of simplicity we might want to employ the same method, just use the @type on choice to distinguish 'proper' corrections from transcription fixes? IMO when such fixes would need to be merged back into TEI source, there would be no associated markup at all - just a change to textual content.

## Decisions made 04-18

* create 3 additional repos: annotation-client, annotation-services, annotation store
* Juri and Joern will compare different webcomponent libraries and create a basic setup in the annotation-client package
* Wolfgang creates a branch on Shakespeare His Contemporaries and changes the ODD to actually output custom elements for tei:w and friends. Helps Northwestern with their questions. Tries to merge Leif's whitespace PR, which will be required for this project.
* Magda checks open TEI-related questions with Martin and looks into possible approaches to standoff annotations, including the one implemented by Jens (https://github.com/jensopetersen/merula)

## Comments from Magda

### Common defects in EEBO texts

#### In-line
- Missing characters `●`
- Missing words `〈◊〉`
- Other short spans `⟨…⟩`
- Longer gaps `<gap extent="1 line"/>`
- Defective spellings `hnsband`
- Wrongly joined words `thyspels`
- Wrongly separated words `be fore`

#### Structural
- stage directions as verse lines `<l>` instead of `<stage>`
- speaker labels as parts of speech `<l>` or `<p>` instead of `<speaker>`
- whole pages missing

### Scope

#### structural stuff out
As Martin explains in [Annotation and discursive boundaries section of his use cases](https://admin.exist-db.org/owncloud/remote.php/webdav/dev/Early%20Modern%20Lab/UsecasescenariosfortheAnnotationModule.pdf) the structural stuff is considered out of scope to cover in full, so it would be sufficient to allow for just flagging up stuff for structural fixes.

#### in-line stuff in
What’s left then would be:
- Transcription/OCR corrections
- All sorts of gaps
- Regularization
- Entities (people & places)
- Other taxonomies (castLists &the like)

The first category (transcription/OCR corrections) is *not* a correction in TEI sense. In TEI sense it is actually a *nothing* and thus would not result in any markup, just changes to textual contents of elements. It still needs an annotation entry though.

### Stand-off

#### Base text

We need base text to anchor annotations. Should this be the current TEI version of EEBO texts? I think not and we have at least 2 options:
1. use the plain text as base text and character offsets for addressing
2. use the tokenized TEI version and xml:ids

Given that we don’t need to worry about structural markup I’m inclined towards 2), at least for the June prototype. In this case I think separating all non-structural TEI (like `<choice>/<orig>|<reg>` into stand-off annotations makes sense, to leave us with a clean slate to merge all annotations into later.

#### Annotations format

I haven’t looked into Web Annotation Data Model yet, but whatever the format is, it needs to cover Martin’s 4W: who, when, what and where. Furthermore, as at some point annotations may be merged back into the source (or structural changes may happen, or whatever), tracking the version of the document makes sense (as a bonus it allows for citing a certain version of a collaboratively curated and changing document).

So annotation entry will end up something like this:
```
Annotation
	documentId
	version
	range (aka where)
	who
	when
	type (correction, regularization, taxonomy etc)
	body (aka what, depends on the type above)
```
*added 9/05*

Tentative proposal for annotation format _export_, should be kosher according to [Web Annotation Data Model](https://www.w3.org/TR/annotation-model/#annotations)

```
{
"id": "http://earlyPrint.org/anno28",
"creator": "http://earlyPrint.org/martin",
"created": "2015-01-28T12:00:00Z",
"modified": "2015-01-29T09:00:00Z",
"generator": "http://earlyPrint.org/ourSnazzyClient",
"generated": "2015-02-04T12:00:00Z",
"body": {
   "type" : "TEI",
   "subtype" : "regularization"
   "text" : "<orig>bleh</orig><reg>blah</reg>",
   "format" : "text/xml"
},
"target": {
   "source": "http://earlyPrint.org/document1.xml",
   "selector": {
      "type": "CSSSelector",
      "value": "#p5",
      "refinedBy": {
         "type": "TextPositionSelector",
         "start": 412,
         "end": 416
      }
   }
}
}
```

##### Interesting bits from above

| Field        |           Example|  Desc |
| ------:        |:-------       :|  -----:|
|source       |http://earlyPrint.org/document1.xml|**where** part 1: document uri|
|creator|http://earlyPrint.org/martin                |**who** aka user id|
|created|2015-01-28T12:00:00Z                ||
|   text|`<orig>bleh</orig><reg>blah</reg>`|**what** or the actual annotation body|
|   subtype|regularization|**annotation type**|
|target|#w5               |**where** part 2: single element or range|

Alternative approach to addressing ranges might be using RangeSelector
```
"selector": {
    "type": "RangeSelector",
    "startSelector": {
        "type": "CSSSelector",
        "value": "#w1"
    },
    "endSelector": {
        "type": "CSSSelector",
        "value": "#w3"
    }
}

```

### Workflow

0. Preprocessing: separate annotation markup, leaving only structural markup behind
1. Merge annotations layer with the current version of base text
2. Display with PM + custom elements, allowing to add annotations per single element see [image](https://admin.exist-db.org/owncloud/remote.php/webdav/dev/Early%20Modern%20Lab/annotator.png)
3. Store edits into annotations layer (& repeat da capo)

### Short-perspective (6 weeks)

Dealing with most common defects in existing transcriptions (character- or word long gaps, defective spellings, wrongly joined/separated words). Requires support for the three edit actions: change, delete, insert; mainly on the `<w>` elements, but `<pc>`, `<w>` and `<gap>`s as well

### Some use cases

*more elaborate use cases in [UseScenarios.md](https://admin.exist-db.org/owncloud/remote.php/webdav/dev/Early%20Modern%20Lab/UseScenarios.md)*

I imagine that all annotations start with a selection: which can be either a single component (e.g. `<w>`) or a continuous run of them. Depending what the selection is, different actions would be available.

#### Simplest (and most common):
- update, insert, split, delete, add word
- merge words

TEI tags: w, pc, c
Due to the fact that whitespace and punctuation is explicitly encoded in the reference corpora, word insertion typically involves at least `<w>` and `<c>` tags, possibly also `<pc>`

Split can be considered as update + insert and merge as update+delete. I believe though that from the users perspective there should be separate functions/forms to take care of that.

#### Phrase-level use cases:
- person, place, commentary, markup error flagging, apparatus entries

TEI tags: persName, placeName, note (for various commentaries, including error flagging; @type can be used to distinguish those), app/rdg

>There was an Old Man of Nantucket

>Who kept all his cash in a bucket.

>His daughter, called Nan,

>Ran away with a man,

>And as for the bucket, Nantucket.

```
<persName ref="#OMoN">
  <w xml:id="w1">Old</w> <w xml:id="w2">Man</w> <w xml:id="w3">of</w> 
  <placeName ref="#Nntkt"><w xml:id="w4">Nantucket</w></placeName>
</persName>
```
```
<persName ref="Nan"><w xml:id="w5">Nan</w></persName>
```

#### Single-word-level:
- edit attributes, regularize, correct, expand

TEI tags: w, choice/(orig|reg|sic|corr|abbr|expan)

Not that much happens below word-level: potential sub-components are thus mainly 3 choice versions (regularization, correction, abbreviation expansion), and only choice/abbr&expan (and precisely its `<abbr>` child) can potentially have one of the other choice components nested. Which is a bit unusual case and we probably don’t need to worry about it straight away, same as with nested choices of the same kind.
In the future potential sub-components are e.g. <g> elements, which may occur anywhere in the word and <ex> elements which may only occur inside <expan>

## Telco 05/23/2016

### Report from iAnnotate

* Access to images: just request them and see if they are available
* Dealing with \<w\>
    * not stripped from the TEI initially
    * every change writes to the change log
* Dynamic cast lists: separate project, probably xforms
    * could also be stored as annotations, but don't need to
* No objections against storing annotations in custom XML format
* Seminar with students starts at June 27

### Backend discussion (Magda's notes)

Two major types of information stored:
1. annotation proper (commentaries and other material external to the source)
2. information that enhances original markup and eventually may be merged into TEI encoding

For the latter, algorithm that performs the merge:
- has to be stable (merge results should be the same irrespective of the way the annotations are stored and as far as possible
of the order they are created/accepted in; what follows, there a) must be some well-specified policy for nesting various types
of annotations and b) source documents need to undergo a normalization process)
- has to perform well enough to process the volume of material we can expect
- has to deal with conflicts.

Potential conflicts:
- annotations are non-mergeable as this would lead to element overlap
- annotations refer to something that was previously deleted (potentially preventable by performing deletes last)

Some arbitrary rules to keep us sane:
- <w> and <pc> elements are considered atomic for the time being so text nodes only, all have @xml:ids
- whitespace wrapped with <c> can be probably ignored
- choice elements can only contain single <w> element
- person, place and app annotations may target consecutive runs of words

Main use cases:
* Type 1:
    * commentaries
        * anchored at the base text
        * anchored at another 
* Type 2 (to be potentially merged into TEI):
    * transcription fixes
    * word correction
        * merge 2 words
        * delete word
        * insert word
    * choice scenarios
        * regularization (orig/reg)
        * correction (sic/corr)
    * entities
        * persons
        * places
    * apparatus variorum

Nesting of use cases illustrated with unlikely example.

Tentative nesting guideline:
    app
    person
    place
    choice
    w

It is unlikely that all will ever co-occur, but let's conceive such an example, where one witness (F1) attests 'twas
a guy called Buckingham (or -hamme) who dun it, while the other witness (F2) points to another one from Worcester.
Both sources can't spell and coincidentally both guys are named after places. Happens all the time. ;-)

```xml
<app>
    <lem wit="F1">
        <persName ref="P123">
            <placeName ref="Bucks">
                <choice>
                    <orig><w>Buckinghamme</w></orig>
                    <reg><w>Buckingham</w></reg>
                </choice>
            </placeName>
        </persName>
    </lem>
    <rdg wit="F2">
        <persName ref="P321">
            <placeName ref="Worcester">
                <choice>
                    <orig><w>Woorchester</w></orig>
                    <reg><w>Worcester</w></reg>
                </choice>
            </placeName>
        </persName>
    </rdg>
</app>
```

Sketch for merge algorithm:

```xquery
for $anno in [applicable annotations]
    group by [annotation target]
    order by 
            1. single targets first
            2. nesting guidelines above (app, person, place, choice, w)
            3. creation date
    try
        replace [annotation target] with [wrapped target]
    catch
        [bloody can't do it, die or something]
```