# review annotation #10

## Goal

At any time when an editor is logged into the system can load a list of not yet
approved annotations and set their state to 'approved' or 'rejected'.

## Actor

editor

## Precondition(s)

* user must be in the 'editor' role and has opened a document.

## Trigger event

editor activates the 'approval' trigger in annotation-list container.

## Success condition

editor changed the state of one or more annotations to 'approved' or 'rejected'.

## Success Scenario

1. editor chooses 'approval' trigger
1. editor gets a list of non-approved annotations displayed in the annotation sidebar
1. editor selects one annotation from the list, gets the suggested changes highlighted and can select
between 'approved' and 'rejected' status.
1. editor confirms the change and the item will disappear from the list.



## UI mockups

optional section to detail the user interface. May link to screenshot or graphical mockups

## Restrictions

Optional:
Any restrictions apply to this use case?

## Variations

The editor uses the search function to display a list of hits within the document
base. Each list entry will display enough context around the actual hit to allow
sensible judgement of the hit relevance. 

The editor is able to approve/reject proposed annotations for the hit list items
displayed.

## Frequency of use

several times a day

Note: e.g. 'once a day', 'once a month', 'dozens of times an hour'. High frequency usually indicates a high priority for the implementation

## Questions

* shall the editor be able to re-visit former status decisions?

## Extensions

Future extensions might support email notification about status changes to the respective
user that proposed an annotation. However this involves additional effort in user 
profile management - the user should be able to opt-out email notification.

