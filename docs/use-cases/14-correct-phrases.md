# Use Case #1

## Goal

short statement 

## Actor

e.g. 'editor'

## Precondition(s)

Optional:
what needs to be in place for this use case to happen 

## Trigger event

how is this use case triggered?

## Success condition

short statement about 'what is the case' when this use case completed


## Success Scenario

step-by-step actions of the user 

1.

## UI mockups

optional section to detail the user interface. May link to screenshot or graphical mockups

## Restrictions

Optional:
Any restrictions apply to this use case?

## Frequency of use

e.g. 'once a day', 'once a month', 'dozens of times an hour'. High frequency usually indicates a high priority for the implementation

