**All scenarios assume reading text display as a starting point**

## Transcription corrections

*user can click on any word and it becomes editable. After single edit is completed, the changes made should stay reflected in the reading text (at least for the current user) and highlighted somehow to signal changes.*

Subcases:
- Update (input contains no whitespace or punctuation, so annotation is limited to the current word)
- Delete (if input is empty)
- Update+Insert (if the input contains whitespace and/or punctuation, new `<w>` (and `<c>` or `<pc>`) elements should be generated

Note: These are most common actions, so **should be really, really smooth** for end users. Inline edits would be optimal.

| Thingy        |           Input|  Markup before|  After |
| ------        |:-------       :|  -----:|
| update        |             bar|`<w xml:id="w1">foo</w>`  | `<w xml:id="w1">bar</w>`|
| delete        |                |`<w xml:id="w1">foo</w>`  |   |
| update/insert |       bar, blah|`<w xml:id="w10">foo</w>` |`<w xml:id="w10">bar</w><pc xml:id="w11">,</pc><c xml:id="w12"> </c><w xml:id="w13">blah</w>`|

## Attributes correction

*Much less common case, I assume. Any custom element corresponding to a TEI element can be selected (upon right click/hover or whatever) and a form comes up that allows to change it's attribute values (not the @xml:id presumably). Changes made (probably?) don't show up in the reading text, but it should become highlighted somehow to signal something has happened and changes should become visible on an annotation list for the element.*

Most common (perhaps even the only one for now) case here would be words

`<w xml:id="w10" lemma="love" ana="#vvz">loue</w>`

So far it looks like 2 attributes really for `<w>`:
- lemma (just text field)
- ana (this is a closed value list, I assume)

## Single-word-level

*On single (but complete) words actions like regularization, abbreviation expansion and marking errors in the source are possible. As it is TEI recommendation (though not requirement) to provide those at a word level, I think thus it makes sense (and we can potentially claim it's actually 'good practice') to allow these only for `<w>` elements. All these require `<choice>` as a wrapper for a pair (or more) elements.*

- regularize (`<orig>` and `<reg>`)
- correct (`<sic>` and `<corr>`)
- expand (`<abbr>` and `<expan>`)

Note: only choice/abbr&expan (and precisely its `<abbr>` child) can potentially have one of the other choice components nested. This is a bit unusual case and we probably don’t need to worry about it straight away, same as with nested choices of the same kind (see complex example below).
In the future potential sub-components are e.g. <g> elements, which may occur anywhere in the word and <ex> elements which may only occur inside <expan>

Markup before 
`<w xml:id="w1">loue</w>`
`<w xml:id="w2">f.</w>`
`<w xml:id="w3">NTAO</w>`

| Thingy    |  After |
| ------    |:  -----|
| regularize|`<w xml:id="w1"><choice><orig>loue</orig><reg>love</reg></w>`|
| correct   |`<w xml:id="w1"><choice><sic>loue</sic><corr>love</corr></w>`|
| expand    |`<w xml:id="w2"><choice><abbr>f.</abbr><expan>foo</expan></w>`|
| complex   |`<w xml:id="w3"><choice><abbr><choice><sic>NTAO</sic><corr>NATO</corr></choice></abbr><expan>North Atlantic Treaty O-thingy</expan></w>`|

**All of the above shouldn't be terribly problematic in the sense that they should never cause overlap headaches.**

**Now on to real fun**

## Phrase-level stuff

*Phrase level is anything that 'can occur at the level of individual words or phrases'  __but__ is still below a structural level (so not a paragraph or a division). In practical terms I think we can allow this on any run of `<w>` or phrase-level siblings(so no overlaps allowed), which should cover vast majority of cases and will not get us into overlap trouble. These things can be nested in any order.*

- people (`<persName>`)
- places (`<placeName>`)

>There was an Old Man of Nantucket

>Who kept all his cash in a bucket.

>His daughter, called Nan,

>Ran away with a man,

>And as for the bucket, Nantucket.

*Punctuation and whitespace tags omitted below for the sake of conciseness*
```
<persName ref="#OMoN">
  <w xml:id="w1">Old</w> <w xml:id="w2">Man</w> <w xml:id="w3">of</w> 
  <placeName ref="#Nntkt"><w xml:id="w4">Nantucket</w></placeName>
</persName>
```
```
<persName ref="#Nan"><w xml:id="w5">Nan</w></persName>
```
*Interesting (and complicated) part here is identifying ppl and places, normally done via @ref attribute. Depending on the type of the source text and approach, this can either link to external authority file (e.g. VIAF for real people) or `<personList>` in the `<teiHeader>` part of the document. Long topic in general, but if we assume we'll get some magic box which can give us a list of persons/places for the document, the input form for this is fairly straightforward.*

- apparatus (`<app>` as a wrapper, with any number of `<rdg>` children and potentially also one `<lem>` child)
 
*In general case, `<app>` entries can nest. In typical case, more interesting bit is probably linking with witnesses that attest a reading (`<rdg>/@wit`), as this assumes having a list of witnesses, so similar to the personList problem.*

## And other phrase-level stuff

*These are different from the other phrase level, that they are not really a part of the text (even if they are encoded inline). So for those, we don't really need to worry about overlap and can allow any span of text as a target.*

- commentaries
- markup errors flagging

# Custom components list
*let's add more as we go along*

## tei-w

editing options:
- inline edits (transcription corrections)
- popups (attribute correction, adding nested component (for now: tei-choice only))

## tei-persName, tei-placeName
*Bagging the two together as they work in a very much the same manner, the only difference being such elements need to be link to different authority lists.*

## tei-choice
*This one comes in 3 flavours as mentioned further above: regularization, correction, expansion. Dunno, perhaps it makes sense to distinguish 3 different components for that at some level? Anyway, a `<tei-choice>` component will always have at least a pair of children elements: `<sic><corr>`, `<orig><reg>` or `<abbr><expan>`. No other pairings are allowed (so no `<sic><expan>` should occur). Choices of different types can be potentially nested, though bar the `complex` example listed further above introducing such options may lead to more confusion than gain.*