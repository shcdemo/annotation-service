# Use Case #1

## Goal

A user registers for aquiring the rights to add annotations, browse personal and
public annotations and store them for further use.

## Actor

user

## Precondition(s)

none

## Trigger event

The user activates the 'register' button or link.

## Success condition

User has a valid username and password to login into the system.

## Success Scenario

step-by-step actions of the user 

1. user selects 'register' link
1. user fills in an email address as username and a password
1. user submits registration form
1. user checks email account for registration confirmation
1. user clicks inline link and will be verified as a new user
1. user visits homepage and uses 'login' link to access the system
1. system will present an 'annotation panel' to interact with the site

## UI mockups

none so far

## Restrictions

no restrictions

## Frequency of use

several times a day

## Open questions

* do email addresses work as a username? (there used to be a problem...)?

## Extensions

* forgotten password: user must have a way to get a password reminder in case the password gets lost
* password change: user should be able to change password.