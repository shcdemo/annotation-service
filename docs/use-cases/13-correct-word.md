# Use Case #1

## Goal

fix the transcription of a word

## Actor

editor

## Precondition(s)

Optional:
- user must be registered to enable this functionality

## Trigger event

user clicks on a word to be corrected

## Success condition

new text value for a word is 
- stored as an annotation
- reflected in the reading version for the current user

## Success Scenario

1. user clicks on a word to be corrected
2. a popup or (better) inline input field is initialized with the current text content of the `<w>` element for the
user to update
3. when edit is complete the user confirms that she wants to commit the change (eg clicking ok or pressing <enter>)
4. as a result of the above user's actions new annotation is stored and the reading text for the edited word remains changed to the new value for the user; the intervention is further signaled via eg highlight

## Test

### Before
`<w xml:id="w123">foo</w>`

joern: to be correct on the clientside this would be:

`<tei-w id="w123">foo</tei-w>`

### After
`<w xml:id="w123">bar</w>`

joern: likewise here:

`<tei-w id="w123">bar</tei-w>`


### Annotation
w/@xml:id
user id
input text

Stub for an annotation to be stored, based on fuller proposal from @joern, leaving here for now the crucial bits [magda]

```xml
<annotation>
    <annotation-body type="TEI" subtype="word-correction" format="text/xml">
        <before>bleh</before>
        <after>blah</after>
    </annotation-body>
    <annotation-target source="sha-ham">
        <selector type="CSSSelector" value="#w5"/>
    </annotation-target>
</annotation>
```
## UI mockups

optional section to detail the user interface. May link to screenshot or graphical mockups

## Restrictions

Optional:
- user must be registered

## Frequency of use

'dozens/hundreds of times an hour'
most common use case in the whole system **absolute top priority** for smooth and flawless functioning