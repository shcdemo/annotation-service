xquery version "3.0";

import module namespace login="http://exist-db.org/xquery/login" at "resource:org/exist/xquery/modules/persistentlogin/login.xql";
import module namespace config="http://existsolutions.com/annotation-service/config" at "modules/config.xqm";


declare variable $exist:path external;
declare variable $exist:resource external;
declare variable $exist:controller external;
declare variable $exist:prefix external;
declare variable $exist:root external;

if ($exist:path eq '') then (
    login:set-user($config:login-domain, (), false()),
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="{request:get-uri()}/"/>
    </dispatch>
) else if ($exist:path eq "/") then (
    login:set-user($config:login-domain, (), false()),
    (: forward root path to index.xql :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="index.html"/>
    </dispatch>
) else if (matches($exist:path, '^/annotation/list')) then (
    login:set-user($config:login-domain, (), false()),
    let $fragments := tokenize(substring-after($exist:path, '/annotation/list/'), '/')[. ne '']
    return
       <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
         <forward url="{$exist:controller}/modules/annotationsList.xqm">
            <add-parameter name="source" value="{$fragments[1]}"/>
            <add-parameter name="fragment" value="{$fragments[2]}"/>
        </forward>
    </dispatch>
) else if (matches($exist:path, '^/annotation/range')) then (
    login:set-user($config:login-domain, (), false()),
    let $fragments := tokenize(substring-after($exist:path, '/annotation/range/'), '/')[. ne '']
    return
       <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
         <forward url="{$exist:controller}/modules/annotationsRange.xqm">
            <add-parameter name="source" value="{$fragments[1]}"/>
            <add-parameter name="start" value="{$fragments[2]}"/>
            <add-parameter name="end" value="{$fragments[3]}"/>
        </forward>
    </dispatch>
) else if (matches($exist:path, '^/annotation/merge')) then (
    login:set-user($config:login-domain, (), false()),
    let $fragments := tokenize(substring-after($exist:path, '/annotation/merge/'), '/')[. ne '']
    return
       <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
         <forward url="{$exist:controller}/modules/merge.xqm">
            <add-parameter name="source" value="{$fragments[1]}"/>
            <add-parameter name="fragment" value="{$fragments[2]}"/>
        </forward>
    </dispatch>
) else if (matches($exist:path, '^/annotation/get')) then (
    login:set-user($config:login-domain, (), false()),
    let $fragments := tokenize(substring-after($exist:path, '/annotation/get/'), '/')[. ne '']
    return
       <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
         <forward url="{$exist:controller}/modules/get.xqm">
            <add-parameter name="id" value="{$fragments[1]}"/>
        </forward>
    </dispatch>
) else if (matches($exist:path, '^/annotation/create')) then (
    login:set-user($config:login-domain, (), false()),
    let $fragments := tokenize(substring-after($exist:path, '/annotation/create/'), '/')[. ne '']
    return
       <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
         <forward url="{$exist:controller}/modules/create.xqm">
        </forward>
    </dispatch>
) else if (matches($exist:path, '^/annotation/update')) then (
    login:set-user($config:login-domain, (), false()),
    let $fragments := tokenize(substring-after($exist:path, '/annotation/update/'), '/')[. ne '']
    return
       <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
         <forward url="{$exist:controller}/modules/update.xqm">
        </forward>
    </dispatch>
) else if (matches($exist:path, '^/annotation/review')) then (
    login:set-user($config:login-domain, (), false()),
    let $fragments := tokenize(substring-after($exist:path, '/annotation/review/'), '/')[. ne '']
    return
       <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
         <forward url="{$exist:controller}/modules/review.xqm">
        </forward>
    </dispatch>
) else if (matches($exist:path, '^/annotation/delete')) then (
    login:set-user($config:login-domain, (), false()),
    let $fragments := tokenize(substring-after($exist:path, '/annotation/delete/'), '/')[. ne '']
    return
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
          <forward url="{$exist:controller}/modules/delete.xqm">
            <add-parameter name="document-id" value="{$fragments[1]}"/>
            <add-parameter name="annotation-id" value="{$fragments[2]}"/>
          </forward>
        </dispatch>
) else if (ends-with($exist:resource, ".html")) then (
    login:set-user($config:login-domain, (), false()),
    (: the html page is run through view.xql to expand templates :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <view>
            <forward url="{$exist:controller}/modules/view.xql"/>
        </view>
		<error-handler>
			<forward url="{$exist:controller}/error-page.html" method="get"/>
			<forward url="{$exist:controller}/modules/view.xql"/>
		</error-handler>
    </dispatch>
) else (
    login:set-user($config:login-domain, (), false()),
    (: everything else is passed through :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <cache-control cache="yes"/>
    </dispatch>
)
