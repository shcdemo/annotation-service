xquery version "3.0";
(:~ 
 : Service for providing the TEI document with 
 : all stand-off annotations brought inline.
 : 
 : @param $source @xml:id of the document
 : @param $version document version number
 : @param $fragment @xml:id of the fragment to be processed (processes the whole document if empty)
 : @param $filter filter criteria to select annotations to be merged
 : @return TEI XML document (or fragment)
 :)

declare namespace tei = "http://www.tei-c.org/ns/1.0";
import module namespace s2i="http://existsolutions.com/annotation-service/standoff2inline" at "standoff2inline.xqm";
import module namespace config="http://existsolutions.com/annotation-service/config" at "config.xqm";
import module namespace annotation="http://existsolutions.com/annotation-service/annotation" at "annotation.xql";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
  
(:    :)
(:let $document-id := request:get-parameter('source', 'Ad9116c1e-c323-431a-a880-b56c08beafcc'):)
(:let $version := request:get-parameter('version', ''):)
(:let $fragment-id := request:get-parameter('fragment', 'A11909_01-e100040'):)

let $document-id := request:get-parameter('source', 'test')
let $version := request:get-parameter('version', '')
let $fragment-id := request:get-parameter('fragment', 'A01353-e100680')
let $generator := annotation:tokenize-params(request:get-parameter('generator', ''))
let $creator := annotation:tokenize-params(request:get-parameter('creator', ''))
let $visibility := annotation:tokenize-params(request:get-parameter('visibility', ''))
let $status := annotation:tokenize-params(request:get-parameter('status', ''))

let $filters := annotation:get-filter-string($generator, $creator, $visibility, $status)

return s2i:merge($document-id, $version, $fragment-id, $filters)