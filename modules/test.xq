xquery version "3.0";
declare namespace tei="http://www.tei-c.org/ns/1.0";

import module namespace config="http://existsolutions.com/annotation-service/config" at "config.xqm";

(:import module namespace annotation="http://existsolutions.com/annotation-service/annotation" at "/db/apps/annotation-service/modules/annotation.xql";:)


let $document-id:= "A01353"
let $version:= "1"
let $start-id:="A01353-000030"
let $end-id:="A01353-000890"

    let $intersect :=  collection($config:normalized-root)//tei:TEI[@xml:id=$document-id][@n=$version]//id($start-id)/following::* intersect
    collection($config:normalized-root)//tei:TEI[@xml:id=$document-id][@n=$version]//id($end-id)/preceding::* 
    
    return $intersect//@xml:id/string()
        
(:    | preceding::$e)//@xml:id/string():)

(:let $b := annotation:get-descendants("Ad9116c1e-c323-431a-a880-b56c08beafcc","1","A11909_01-e100050"):)
(::)
(:return collection("/db/apps/shctexts/data/annotations/Ad9116c1e-c323-431a-a880-b56c08beafcc")//eml-annotation-target[@version="1"][@source="Ad9116c1e-c323-431a-a880-b56c08beafcc"][selector/@value=$b]/parent::eml-annotation:)

(:return collection("/db/apps/shctexts/data/annotations/Ad9116c1e-c323-431a-a880-b56c08beafcc")//eml-annotation-target[@version="1"][@source="Ad9116c1e-c323-431a-a880-b56c08beafcc"][selector/@value=annotation:get-descendants("Ad9116c1e-c323-431a-a880-b56c08beafcc","1","A11909_01-e100050")]/parent::eml-annotation:)
  
(:string-join(annotation:get-descendants("Ad9116c1e-c323-431a-a880-b56c08beafcc","1","A11909_01-e100050"), '", "'):)
(::)
(:let $descendants := annotation:get-descendants("Ad9116c1e-c323-431a-a880-b56c08beafcc","1","A11909_01-e100040") :)
(:return $descendants:)
(:return collection("/db/apps/shctexts/data/annotations/Ad9116c1e-c323-431a-a880-b56c08beafcc")//eml-annotation-target[@version="1"][@source="Ad9116c1e-c323-431a-a880-b56c08beafcc"][selector/@value=$descendants]:)