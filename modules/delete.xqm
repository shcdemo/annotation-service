xquery version "3.0";

import module namespace annotation="http://existsolutions.com/annotation-service/annotation" at "annotation.xql";
import module namespace config="http://existsolutions.com/annotation-service/config" at "config.xqm";
import module namespace login="http://exist-db.org/xquery/login" at "resource:org/exist/xquery/modules/persistentlogin/login.xql";
import module namespace normalization="http://existsolutions.com/annotation-service/normalization" at "normalization.xqm";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";

declare namespace tei = "http://www.tei-c.org/ns/1.0";


(:let $setuser :=  login:set-user($config:login-domain, (), false()) :)
let $user := request:get-attribute($config:login-domain || ".user")
let $data := request:get-data()
let $document-id := request:get-parameter('document-id', '')
let $annotation-id := request:get-parameter('annotation-id', '')

let $annotation := annotation:annotation-by-id($document-id, $annotation-id)

let $creator := if($annotation) then $annotation/@creator else ''
let $authorized := annotation:authorized($user, $creator, $document-id, $annotation-id, 'create')

return if ($annotation)
    then
        if($authorized='OK') then
            annotation:delete-by-id($document-id, $annotation-id)
        else
            $authorized
    else (
         response:set-status-code(200),
         <response>no annotation found</response>
         )
