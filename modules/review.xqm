xquery version "3.0";

import module namespace annotation="http://existsolutions.com/annotation-service/annotation" at "annotation.xql";
import module namespace config="http://existsolutions.com/annotation-service/config" at "config.xqm";
import module namespace login="http://exist-db.org/xquery/login" at "resource:org/exist/xquery/modules/persistentlogin/login.xql";
import module namespace normalization="http://existsolutions.com/annotation-service/normalization" at "normalization.xqm";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";

declare namespace tei = "http://www.tei-c.org/ns/1.0";

let $template :=
    <annotation-item id="Adb03e249-ae6c-4e25-bbce-c1f271012045" reason="" creator="earlyPrint" visibility="public" generator="import" status="accepted" created="2015-01-01">
        <annotation-body type="TEI" subtype="regularization" format="text/xml">
            <orig>Enterlude</orig>
            <reg>Interlude</reg>
        </annotation-body>
        <annotation-target source="test" version="1">
            <target-selector type="IdSelector" value="A01353-000050"/>
        </annotation-target>
    </annotation-item>

let $setuser :=  login:set-user($config:login-domain, (), false()) 
let $user := request:get-attribute($config:login-domain || ".user")
let $data := request:get-data()
(:let $user :='shcadmin':)
(:let $data := $template:)
let $request-user := if($data) then $data/@creator else ''
let $document-id := if($data) then $data//annotation-target/@source else ''

let $authorized := annotation:authorized($user, $request-user, $document-id, '', 'review')

return if ($data)
    then
        if($authorized='OK') then
            annotation:review($user, normalization:cleanup-client-data($data))
        else
            $authorized
    else (
         response:set-status-code(400),
         <error><reason>could not read request data</reason><code>1</code></error>
         )
