xquery version "3.0";
(:~ 
 : Service for providing filtered annotations list for arbitrary document chunk
 : 
 : @param $source @xml:id of the document
 : @param $version document version number
 : @param $start @xml:id of the starting point in a document
 : @param $end @xml:id of the end point in a document
 : @param $filter filter criteria
 : @return list of annotations
 :)

declare namespace tei = "http://www.tei-c.org/ns/1.0";
import module namespace s2i="http://existsolutions.com/annotation-service/standoff2inline" at "standoff2inline.xqm";
import module namespace annotation="http://existsolutions.com/annotation-service/annotation" at "annotation.xql";
import module namespace config="http://existsolutions.com/annotation-service/config" at "config.xqm";

let $document-id := request:get-parameter('source', 'A01353')
let $version := request:get-parameter('version', '1')
let $start-id := request:get-parameter('start', 'A01353-000030')
let $end-id := request:get-parameter('end', 'A01353-000890')
let $generator := annotation:tokenize-params(request:get-parameter('generator', 'import'))
let $creator := annotation:tokenize-params(request:get-parameter('creator', 'earlyPrint daemon'))
let $visibility := annotation:tokenize-params(request:get-parameter('visibility', 'public private'))
let $status := annotation:tokenize-params(request:get-parameter('status', ''))

let $filters := annotation:get-filter-string($generator, $creator, $visibility, $status)

return annotation:get-annotations-for-range($document-id, $version, $start-id, $end-id, $filters)