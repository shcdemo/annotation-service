xquery version "3.0";
(:~ 
 : Service for importing TEI documents for annotation
 : Imported documents will be
 : <ul>
 : <li>normalized and have annotations extracted to `annotations` before storing in `normalized` subcollection</li>
 : <li>moved to a `texts` subcollection</li>
 : </ul>
 : 
 : Appropriate subcollection structure will be created if not in place already. 
 : Only documents directly in a source collection will be imported, subcollections are ignored.
 : 
 : @param $source path to the collection to be imported
 :)

declare namespace tei = "http://www.tei-c.org/ns/1.0";
import module namespace import="http://existsolutions.com/annotation-service/import" at "import.xql";
import module namespace config="http://existsolutions.com/annotation-service/config" at "config.xqm";

let $source := request:get-parameter('source', $config:data-root)
let $text-root := $source || '/import'
let $anno-root := $source || '/annotations'
let $normalized-root := $source || '/texts'

(: set up expected subcollections :)
let $text := if(xmldb:collection-available($text-root)) then () else xmldb:create-collection($source, 'import')
let $anno := if(xmldb:collection-available($anno-root)) then () else xmldb:create-collection($source, 'annotations')
let $normalized := if(xmldb:collection-available($normalized-root)) then () else xmldb:create-collection($source, 'texts')

return
    for $doc in xmldb:xcollection($source) 
        return (import:import($doc, $anno-root, $normalized-root, $text-root), 
                xmldb:move($source, $text-root, util:document-name($doc/tei:TEI))
                )