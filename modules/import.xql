xquery version "3.0";
(:~
 : Functions for importing TEI files into structures expected by annotation module 
:)

module namespace import="http://existsolutions.com/annotation-service/import";

import module namespace config="http://existsolutions.com/annotation-service/config" at "config.xqm";
import module namespace i2s="http://existsolutions.com/annotation-service/inline2standoff" at "inline2standoff.xqm";
import module namespace normalization="http://existsolutions.com/annotation-service/normalization" at "normalization.xqm";

declare namespace tei = "http://www.tei-c.org/ns/1.0";

(:~ 
 : Imports a TEI document, separating annotations and base text. A document will
 : <ul>
 : <li>be given an @xml:id if not already present</li>
 : <li>dots etc stripped from @xml:ids</li>
 : <li>have some kinds of markup stripped off and stored in `anno-root` (@see upcoming docs for rationale which kinds of markup are subject to be stood-off)</li> 
 : <li>undergo a normalization process and stored in `normalized-root` collection</li>
:)
declare function import:import($doc as node(), $anno-root as xs:string, $normalized-root as xs:string, $text-root as xs:string) {
        (: make sure a document has TEI/@xml:id :)
        let $id := if($doc/tei:TEI/@xml:id) then () else update insert attribute xml:id {'A' || util:uuid()} into $doc/tei:TEI
        
        let $annotations:= <annotation-list>{i2s:extract($doc//tei:text)}</annotation-list>
        let $ndoc:= normalization:normalize($doc/tei:TEI)
        return 
            (if(xmldb:collection-available($anno-root || '/' || $doc/tei:TEI/@xml:id/string())) 
                then xmldb:store($anno-root || '/' || $doc/tei:TEI/@xml:id/string(), "annotations.xml", $annotations)
                else (
                    xmldb:create-collection($anno-root, $doc/tei:TEI/@xml:id/string()),
                    xmldb:store($anno-root || '/' || $doc/tei:TEI/@xml:id/string(), "annotations.xml", $annotations)
                    ),
            xmldb:store($normalized-root, util:document-name($doc/tei:TEI), $ndoc)
            )
};