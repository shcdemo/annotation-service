xquery version "3.0";

module namespace s2i="http://existsolutions.com/annotation-service/standoff2inline";

import module namespace annotation="http://existsolutions.com/annotation-service/annotation" at "annotation.xql";
import module namespace config="http://existsolutions.com/annotation-service/config" at "config.xqm";
import module namespace functx = "http://www.functx.com";

 import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";

declare namespace tei = "http://www.tei-c.org/ns/1.0";

declare function s2i:get-fragment($document-id as xs:string, $version as xs:string?, $fragment-id as xs:string?) {
    let $doc := collection($config:normalized-root || "/" || substring($document-id, 1, 3))/id($document-id)

    return if($fragment-id) then $doc//id($fragment-id) else $doc
};

declare function s2i:get-annotations($document-id as xs:string, $version as xs:string?, $fragment-id as xs:string?, $filters as xs:string?) as node()* {
    let $annotations:= doc($config:anno-root || "/" || substring($document-id, 1, 3) || "/" || $document-id || "_annotations.xml")//annotation-item
    return $annotations
};

declare function s2i:get-target-annotations($document-id as xs:string, $version as xs:string?, $target-id as xs:string, $filters as xs:string?) as node()* {
    if ($filters) then
        let $query := '
            doc("' || $config:anno-root || "/" || substring($document-id, 1, 3) || "/" || $document-id || "_annotations.xml" || '")//annotation-target'
                || '/parent::annotation-item[annotation-target/target-selector/@value = $target-id]'
        return util:eval($query)
    else
        doc($config:anno-root || "/" || substring($document-id, 1, 3) || "/" || $document-id || "_annotations.xml")//annotation-item[annotation-target/target-selector/@value = $target-id][@status != "rejected"]
};

declare function s2i:merge($document-id as xs:string, $version as xs:string?, $fragment-id as xs:string?, $filters as xs:string?) {
 let $start := util:system-time()
    let $node := s2i:get-fragment($document-id, $version, $fragment-id)
    let $annotations := doc($config:anno-root || "/" || substring($document-id, 1, 3) || "/" || $document-id || "_annotations.xml")//annotation-item[@status != "rejected"][.//target-selector/@type='IdSelector']
    let $annotationsMulti := doc($config:anno-root || "/" || substring($document-id, 1, 3) || "/" || $document-id || "_annotations.xml")//annotation-item[@status != "rejected"][.//target-selector/@type='RangeSelector']
    let $annotationsMap := s2i:annotations-map($annotations)
    let $annotationsMapMulti := s2i:annotations-map-multi($annotationsMulti)
    (: let $getAnnotations := s2i:get-target-annotations($document-id, $version, ?, $filters) :)
    return (s2i:expand-annotations(s2i:expand-annotations-multi($node, $annotationsMapMulti), $annotationsMap),
 console:log("merge took " || (util:system-time() - $start)))
};

declare function s2i:merge($fragment as node(), $filters as xs:string?) {
    (: let $start := util:system-time() :)
    let $document-id := root($fragment)/tei:TEI/@xml:id
    let $version := root($fragment)/tei:TEI/@n
    let $annotations := doc($config:anno-root || "/" || substring($document-id, 1, 3) || "/" || $document-id || "_annotations.xml")//annotation-item[@status != "rejected"][.//target-selector/@type='IdSelector']
    let $annotationsMap := s2i:annotations-map($annotations)
    let $annotationsMulti := doc($config:anno-root || "/" || substring($document-id, 1, 3) || "/" || $document-id || "_annotations.xml")//annotation-item[@status != "rejected"][.//target-selector/@type='RangeSelector']
    let $annotationsMapMulti := s2i:annotations-map-multi($annotationsMulti)
    (: let $getAnnotations := s2i:get-target-annotations($document-id, $version, ?, $filters) :)
    return (s2i:expand-annotations(s2i:expand-annotations-multi($fragment, $annotationsMapMulti), $annotationsMap)
(:        s2i:expand-annotations($fragment, $annotationsMap):)
(: console:log("merge took " || (util:system-time() - $start)) :)
    )
};


(:~
 : Merge annotations into the chunk delimited by two milestone elements and return
 : the fragment resulting from it.
 :)
declare function s2i:merge-milestone-chunk($root as document-node(), $ms1 as element(), $ms2 as element()?, $node as node()*) {
(: let $start := util:system-time() :)
    let $document-id := $root/tei:TEI/@xml:id
    let $version := $root/tei:TEI/@n
    (: let $annotations := doc($config:anno-root || "/" || substring($document-id, 1, 3) || "/" $document-id || "_annotations.xml")//annotation-item[@status = "accepted"]
    let $annotationsMap := s2i:annotations-map($annotations) :)
    let $getAnnotations := s2i:get-target-annotations($document-id, $version, ?, '')
    let $annotationsMulti := doc($config:anno-root || "/" || substring($document-id, 1, 3) || "/" || $document-id || "_annotations.xml")//annotation-item[@status != "rejected"][.//target-selector/@type='RangeSelector']
    let $getAnnotationsMulti := s2i:annotations-map-multi($annotationsMulti)
    return (
        s2i:expand-annotations(s2i:combine-milestone-chunk($ms1, $ms2, $node, $getAnnotations, $getAnnotationsMulti), $getAnnotations)
(: , console:log("merge took " || (util:system-time() - $start)) :)
    )
};

declare %private function s2i:annotations-map($annotationItems as node()*) {
    map:merge(
        for $annotation in $annotationItems
        return
            map:entry($annotation/annotation-target/target-selector/@value, $annotation)
    )
};

declare %private function s2i:annotations-map-multi($annotationItems as node()*) {
    let $c:= console:log(count($annotationItems))
    return
    map:merge(
        for $annotation in $annotationItems
        let $source := $annotation/annotation-target/@source
        let $start := $annotation/annotation-target/target-selector/start-selector/@value
        let $endValue := $annotation/annotation-target/target-selector/end-selector/@value
        let $end:= if(matches($endValue, '.*-s-[1-9]')) then functx:substring-before-last($endValue, '-s-') else $endValue

        let $lca := s2i:lca(collection($config:normalized-root)//id($source)//id($start), collection($config:normalized-root)//id($source)//id($end))
(:        let $c:= console:log($lca):)
        group by $lca
        let $c:= console:log('ann count' || count($annotation))
        let $c:= console:log($annotation)
        
(:        let $c:= console:log('source ' || $source || ': ' || $start || ' -- ' || $end ):)
        return
            map:entry($lca, $annotation)
    )
};

declare function s2i:lca($first, $second) {
    let $intersection := $first/ancestor-or-self::* intersect $second/ancestor-or-self::*
(:    let $v := console:log(string-join($intersection ! @xml:id, ' ')):)
    let $last := subsequence($intersection, count($intersection))
        
    return if($last/@xml:id) then $last/@xml:id else $last/ancestor::*[@xml:id][1]/@xml:id
};

declare function s2i:wrap($node, $anno) {
    switch ($anno//annotation-body/@subtype)
        case "delete" return s2i:delete($node, $anno)
        case "update" return s2i:transcription($node, $anno)
        case "join" return s2i:join($node, $anno)
        case "split" return s2i:split($node, $anno//annotation-body/element(), $anno)
        case "regularization" return s2i:regularization($node, $anno)
        case "person" return s2i:person($node, $anno)
        case "update.attribute" return s2i:attribute($node, $anno)
        default return $node
};

declare function s2i:attribute($node, $anno) {
    element { node-name ($node) } {
        $node/@*[name(.) != $anno//attribute/@name],
        attribute { $anno//attribute/@name } { $anno//attribute/string() },
        $node/node()
    }
};

declare function s2i:delete($node, $anno) {
(:    let $status := $anno/@status:)
    ()
};


declare function s2i:transcription($node, $anno) {
    let $body := $anno//annotation-body/*/string()
    return
        <w xmlns="http://www.tei-c.org/ns/1.0">
                {for $att in $node/@*
                   return $att
                ,
                $body}
        </w>
};

declare function s2i:regularization($node, $anno) {
    (: original word might already be wrapped in a choice :)
    let $origW := if ($node/name() = 'choice')
                then ($node/*:orig/*:w)
                else ($node)

    let $wordId := $origW/@xml:id/string()
    let $regularized := $anno//*:reg/string()
    let $attributes := (
            $origW/@*[not(./name() = ('xml:id','reg'))],
            (attribute reg { $regularized })
        )

    (: choice/@xml:id check to prevent duplicate ids on roundtrip extract-merge :)
    let $choiceId := if ($anno/@id = $wordId) then 'A' || util:uuid() else $anno/@id

    return <choice xmlns="http://www.tei-c.org/ns/1.0">
              {attribute xml:id {$choiceId}}
                <orig><w>
                {
                    ($attributes, (attribute xml:id { $wordId })),
                    $origW/string()
                }
                </w></orig>
                <reg><w>
                {
                    ($attributes, (attribute xml:id { $wordId || '-reg' })),
                    $regularized
                }
                </w></reg>
           </choice>
};

declare function s2i:person($node, $anno) {
    <persName xmlns="http://www.tei-c.org/ns/1.0">
        {attribute ref {$anno//*:ref/string()}}
        {attribute xml:id {$anno/@id}}
        {$node}
    </persName>
};

(:~
 : was: Merge annotations for the chunk delimited by two milestone elements, $ms1 and $ms2.
 : Return a new fragment containing the merged annotations.
 : 
 : note to self:
 : this needs to be refactored as current approach is first to recreate the well-formed xml fragment from thechunk of the original document between the milestones
 : then apply multi-target annotations on it and return
 : while single-target annotations are applied afterwards in a call from merge-milestone-chunk
 : so some renaming and splitting of functions would be in order here but no time for it atm
 :)
(: :     return (s2i:expand-annotations(s2i:expand-annotations-multi($node, $annotationsMapMulti), $annotationsMap),:)
declare function s2i:combine-milestone-chunk($ms1 as element(), $ms2 as element()?, $node as node()*, $getAnnotations as function(*), $getAnnotationsMulti as function(*)) as node()*
{
    let $chunk :=
        typeswitch ($node)
            case element() return
                if ($node is $ms1) then
                    util:expand($node, "add-exist-id=all")
                else if ( some $n in $node/descendant::* satisfies ($n is $ms1 or $n is $ms2) ) then
                    element { node-name($node) } {
                        $node/@*,
                        for $i in ( $node/node() )
                        return s2i:combine-milestone-chunk($ms1, $ms2, $i, $getAnnotations, $getAnnotationsMulti)
                    }
                else if ($node >> $ms1 and (empty($ms2) or $node << $ms2)) then
                    $node 
                else
                    ()
            case attribute() return
                $node (: will never match attributes outside non-returned elements :)
            default return
                if ($node >> $ms1 and (empty($ms2) or $node << $ms2)) then
                    $node
                else
                    ()
(:    let $c:=console:log($chunk):)
    return 
(:        s2i:expand-annotations(s2i:expand-annotations-multi($chunk, $getAnnotationsMulti), $getAnnotations):)
        s2i:expand-annotations-multi($chunk, $getAnnotationsMulti)

};

declare function s2i:wrap-recursive-multi($node, $anno) {
    if(count($anno)>1) then
        let $tail := subsequence($anno, 2)
        return s2i:wrap-recursive-multi(s2i:wrap-multi($node, $anno[1]), $tail)
    else
        s2i:wrap-multi($node, $anno)
};


(:~ Wrap a sequence of child nodes
 : TODO
 : 1. check for conflicts
 : 2. properly group and nest multiple multi-target annotations
 :  :)
declare function s2i:wrap-multi($parent, $anno) {

    let $start := $anno//start-selector/@value
    let $end := $anno//end-selector/@value

    let $targets :=
         $parent//*[@xml:id=$start] |
        ($parent//*[@xml:id=$start]/following::node() intersect $parent//*[@xml:id=$end]/preceding::node()) |
         $parent//*[@xml:id=$end]

(:    let $g := console:log('wrap-multi ' || $start || ' - ' || $end || ' tar: ' || count($targets)):)

    return
    element { node-name($parent) } {
        $parent/@*,
        for $node in $parent/node()
            return
                typeswitch($node)
                    case element()
                        return
                            if ($node/@xml:id=$start) then
(:                                let $c:=console:log('s2i:wrap ' || $node/@xml:id || ' ' || $anno//start-selector/@value || ' - ' || $anno//end-selector/@value || ' ' || $anno//annotation-body/@subtype || $anno//annotation-body/string() || ' targets: '):)
(:                                let $g := console:log($targets):)
(:                                return:)
                                s2i:wrap($targets, $anno)
                            else if ($node intersect $targets) then
                                ()
                            else
                                $node
                    default
                        return $node
    }
};

declare function s2i:expand-annotations-multi($nodes as node()*, $getAnnotations as function(*)) {
    for $node in $nodes
    return
        typeswitch ($node)
            case document-node() return
                s2i:expand-annotations-multi($node/*, $getAnnotations)
(:            case element(tei:w) return $node:)
            case element() return
                let $id := $node/@xml:id
                return
                    if ($id) then
                        let $ann := $getAnnotations($id)
                        return
                            if (count($ann)) then
                                s2i:wrap-recursive-multi($node, $ann)
                            else
                                s2i:process-children-multi($node, $getAnnotations)
                    else
                        s2i:process-children-multi($node, $getAnnotations)
            default return
                $node
};

declare function s2i:expand-annotations($nodes as node()*, $getAnnotations as function(*)) {
    for $node in $nodes
    return
        typeswitch ($node)
            case document-node() return
                s2i:expand-annotations($node/*, $getAnnotations)
            case element(tei:w) return
                let $id := $node/@xml:id
                return
                    if ($id) then
                        let $ann := $getAnnotations($id)
                        return
                            if (exists($ann)) then
                                let $anno :=
                                    for $a in $ann
                                    order by $config:nesting($a//annotation-body/@subtype)
                                    return $a
                                return
                                    s2i:wrap-recursive($node, $anno)
                            else
                                s2i:process-children($node, $getAnnotations)
                    else
                        s2i:process-children($node, $getAnnotations)
            case element() return
                s2i:process-children($node, $getAnnotations)
            default return
                $node
};

declare function s2i:wrap-recursive($node, $anno) {
    if(count($anno)>1) then
        let $tail := subsequence($anno, 2)
        return s2i:wrap-recursive(s2i:wrap($node, $anno[1]), $tail)
    else
        s2i:wrap($node, $anno)
};

declare function s2i:process-children($node as element(), $getAnnotations as function(*)) {
    element { node-name($node) } {
        $node/@*,
        s2i:expand-annotations($node/node(), $getAnnotations)
    }
};

declare function s2i:process-children-multi($node as element(), $getAnnotations as function(*)) {
    element { node-name($node) } {
        $node/@*,
        s2i:expand-annotations-multi($node/node(), $getAnnotations)
    }
};

declare function s2i:join($nodes, $anno) {
    <w xmlns="http://www.tei-c.org/ns/1.0">
            {for $att in $nodes[1]/@*
               return $att
            ,
            $anno//*:w/string()}
    </w>
};

declare function local:getAttributesForSplitNode($origNode, $node, $pos) {
  if (local-name($node) = 'c')  (: suppress attributes on c elements :)
  then ()
  else (
    if ($pos = 1)               (: first word inherits original attributes :)
    then ($origNode/@*)
    else (
      if ($node/@xml:id)        (: ensure xml:id is set for each new element :)
      then ($node/@*)
      else ($node/@*, attribute xml:id {$origNode/@xml:id || "-s-" || $pos})
    )
  )
};

declare function s2i:split($origNode, $nodes, $anno) {
    (: unwrap word from choice, if necessary :)
    let $original := if (local-name($origNode) = 'choice')
                     then ($origNode/*:orig/*:w)
                     else ($origNode)

    for $node at $pos in $nodes
     let $newNode :=   
        element { 'tei:' || node-name($node) } {
            local:getAttributesForSplitNode($original, $node, $pos),
            $node/node()
        }
        
    return
        if($pos>1) then 
            let $annotations := collection($config:anno-root || "//" || $anno//annotation-target/@source/string())//annotation-item[@status != "rejected"][.//target-selector[@value=$newNode/@xml:id]]
            let $anno :=
                for $a in $annotations
                order by $config:nesting($a//annotation-body/@subtype)
                return $a
                
            return s2i:wrap-recursive($newNode, $anno)
(:            return $newNode:)
        else
            $newNode
};