xquery version "3.0";

import module namespace annotation="http://existsolutions.com/annotation-service/annotation" at "annotation.xql";

declare namespace tei = "http://www.tei-c.org/ns/1.0";

let $ann-id := request:get-parameter('id', '898b1b1d-8b2d-442d-9002-4ff382a5a57c')

return annotation:annotation-by-id($ann-id)