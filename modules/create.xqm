xquery version "3.0";

import module namespace annotation="http://existsolutions.com/annotation-service/annotation" at "annotation.xql";
import module namespace config="http://existsolutions.com/annotation-service/config" at "config.xqm";
import module namespace login="http://exist-db.org/xquery/login" at "resource:org/exist/xquery/modules/persistentlogin/login.xql";
import module namespace normalization="http://existsolutions.com/annotation-service/normalization" at "normalization.xqm";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";

declare namespace tei = "http://www.tei-c.org/ns/1.0";

let $template := <annotation-item temp-id="BLAH_32f598e4" creator="earlyPrint" visibility="public" generator="test" status="accepted">
    {attribute created {substring(string(current-date()), 1, 10)}}
    <annotation-body type="TEI" subtype="regularization" format="text/xml">
        <orig>Wyfe</orig>
        <reg>Wife{current-time()}</reg>
    </annotation-body>
    <annotation-target source="Ad9116c1e-c323-431a-a880-b56c08beafcc" version="1">
        <target-selector type="IdSelector" value="A11909_01-000130"/>
    </annotation-target>
</annotation-item>

(:let $setuser :=  login:set-user($config:login-domain, (), false()) :)
let $user := request:get-attribute($config:login-domain || ".user")
let $data := request:get-data()
(:let $c:=console:log(sm:id()):)
(:let $user :='earlyPrint':)
(:let $data := $template:)
let $request-user := if($data) then $data/@creator else ''
let $document-id := if($data) then $data//annotation-target/@source else ''

let $authorized := annotation:authorized($user, $request-user, $document-id, '', 'create')

return if ($data)
    then
        if($authorized='OK') then
            annotation:create($document-id, normalization:cleanup-client-data($data))
        else
            $authorized
    else (
         response:set-status-code(400),
         <error><reason>could not read request data</reason><code>1</code></error>
         )
