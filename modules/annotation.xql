xquery version "3.0";
(:~
 : Functions for storing, updating and retrieving annotations expected by annotation module
:)

module namespace annotation="http://existsolutions.com/annotation-service/annotation";

import module namespace config="http://existsolutions.com/annotation-service/config" at "config.xqm";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace functx = "http://www.functx.com";

declare namespace tei = "http://www.tei-c.org/ns/1.0";

declare function annotation:annotation-by-id($document-id as xs:string, $ann-id as xs:string) {
    let $ann := doc($config:anno-root || "/" || substring($document-id, 1, 3) || "/" || $document-id || "_annotations.xml")//annotation-item[@id=$ann-id]
    return
        if($ann) then (annotation:status('OK'), $ann) else annotation:status('ERR_NOT_FOUND')
};

declare function annotation:delete-by-id($document-id as xs:string, $ann-id as xs:string) {
    let $ann := doc($config:anno-root || "/" || substring($document-id, 1, 3) || "/" || $document-id || "_annotations.xml")//annotation-item[@id=$ann-id][.//annotation-target/@source=$document-id]
    let $delete := update delete $ann
    let $deleted := doc($config:anno-root || "/" || substring($document-id, 1, 3) || "/" || $document-id || "_annotations.xml")//annotation-item[@id=$ann-id][.//annotation-target/@source=$document-id]
    return
        if($deleted) then annotation:status('ERR_DELETE') else annotation:status('OK')
};

declare function annotation:annotation-creator($document-id, $annotation-id) {
    let $ann := annotation:annotation-by-id($document-id, $annotation-id)
    return if($ann) then $ann/@creator else ()
};

declare function annotation:authorized($user, $request-user, $document-id, $annotation-id, $action) {
    if(not($user)) then
        annotation:status('ERR_FORBIDDEN', <data><reason>not logged in</reason></data>)
    else
        if($action!='review' and $user ne $request-user) then
            annotation:status('ERR_FORBIDDEN', <data><reason>users don't match</reason></data>)
        else
            if(sm:user-exists($user) and annotation:check-group(sm:get-user-groups($user), $action)) then
                if($action=('update', 'delete')) then
                    let $creator := annotation:annotation-creator($document-id, $annotation-id)
                    return
                    if ($user = $creator) then
                        'OK'
                    else
                        annotation:status('ERR_FORBIDDEN', <data><reason>{$user} not owner ({$creator})</reason></data>)
                else
                    'OK'
            else
                annotation:status('ERR_FORBIDDEN', <data><reason>group invalid</reason></data>)
};

declare function annotation:check-group($group, $action) {
            switch ($action)
                case "create" return if($config:user-group=$group or $config:admin-group=$group) then true() else ()
                case "update" return if($config:user-group=$group or $config:admin-group=$group) then true() else ()
                case "delete" return if($config:user-group=$group or $config:admin-group=$group) then true() else ()
                case "review" return if($config:admin-group=$group) then true() else ()
                default return ()
};

declare function annotation:review($reviewer-id as xs:string, $data as node()) {
    let $document-id:=$data/descendant-or-self::annotation-target/@source
    let $annotation-id:=$data/descendant-or-self::annotation-item/@id
    let $status:=$data/descendant-or-self::annotation-item/@status
    let $reason :=$data/descendant-or-self::annotation-item/@reason

    return
    if($document-id) then
        let $path := $config:anno-root || '/' || substring($document-id, 1, 3) || '/' || $document-id || '_annotations.xml'
        let $doc:=if(doc($path)) then doc($path) else annotation:setup($document-id)
        let $insert:= update replace $doc/descendant-or-self::annotation-item[@id=$annotation-id]/@status with $status
        let $insert:= update replace $doc/descendant-or-self::annotation-item[@id=$annotation-id]/@reason with $reason
        let $insert:= update replace $doc/descendant-or-self::annotation-item[@id=$annotation-id]/@modifier with $reviewer-id
        let $now := xs:string(adjust-dateTime-to-timezone(current-dateTime(), xs:dayTimeDuration("PT0S")))
        let $insert:= update replace $doc/descendant-or-self::annotation-item[@id=$annotation-id]/@modified with $now

        let $anno := $doc//annotation-item[@id=$annotation-id]
        return if($anno/@status=$status) then annotation:status('OK', $anno) else annotation:status('ERR_UPDATE')
    else annotation:status('ERR_DOC')
};

declare function annotation:update($user-id as xs:string, $data as node()) {
    let $document-id:=$data/descendant-or-self::annotation-target/@source
    let $annotation-id:=$data/descendant-or-self::annotation-item/@id

    let $item := functx:add-or-update-attributes($data/descendant-or-self::annotation-item,
                                                 xs:QName("modifier"),
                                                 $user-id)
    let $item := functx:add-or-update-attributes($item,
                                                 xs:QName("modified"),
                                                 xs:string(adjust-dateTime-to-timezone(current-dateTime(),
                                                                                       xs:dayTimeDuration("PT0S"))))
    return
    if($document-id) then
        let $path := $config:anno-root || '/' || substring($document-id, 1, 3) || '/' || $document-id || '_annotations.xml'
        let $doc:=if(doc($path)) then doc($path) else annotation:setup($document-id)
        let $insert:= update replace $doc/descendant-or-self::annotation-item[@id=$annotation-id] with $item

        let $anno := $doc//annotation-item[@id=$annotation-id]
        return if($anno) then annotation:status('OK', $anno) else annotation:status('ERR_UPDATE')
    else annotation:status('ERR_DOC')
};

declare function local:replace-annotation-body($element as element(), $body as element()) {
  if ($element/self::annotation-body)
  then $body
  else element {node-name($element)}
               {$element/@*,
                for $child in $element/node()
                return if ($child instance of element())
                       then local:replace-annotation-body($child, $body)
                       else $child
               }
};

(:
 : Check whether the word(s) specified in the annotation target selector(s) refer
 : to words that actually exist in the document.  This mostly avoids storing bogus
 : "tmp-" word IDs generated by the client.
 :)
declare function annotation:validate-targets($anno as node()) as xs:boolean {
    let $source := $anno/annotation-target/@source/string()
    let $anno_start := if ($anno/annotation-target/target-selector/@type eq 'RangeSelector')
                       then $anno/annotation-target/target-selector/start-selector/@value/string()
                       else $anno/annotation-target/target-selector/@value/string()
    let $anno_end := if ($anno/annotation-target/target-selector/@type eq 'RangeSelector')
                     then $anno/annotation-target/target-selector/end-selector/@value/string()
                     else $anno/annotation-target/target-selector/@value/string()

    (: Attribute updates to all but the first word in a split will have something appended
     : to the ID, e.g., A21328-001-b-0620-s-3, because the actual word does not exist yet,
     : so chop off that part and only check the word we already have.
     :)
    let $anno_start :=  if ($anno/annotation-body/@subtype eq "update.attribute")
                        then (fn:replace($anno_start, "-s-\d+$", ""))
                        else $anno_start
    let $anno_end :=  if ($anno/annotation-body/@subtype eq "update.attribute")
                        then (fn:replace($anno_end, "-s-\d+$", ""))
                        else $anno_end

    let $doc := doc($config:texts-root || '/' || substring($source, 1, 3) || '/' || $source || '.xml')
    let $w_start := $doc/id($anno_start)
    let $w_end := $doc/id($anno_end)
    return (exists($w_start) and exists($w_end))
};

declare function annotation:create($document-id as xs:string?, $data as node()?) {
    if($document-id) then
        let $id := "A" || util:uuid()
        let $anno := functx:add-or-update-attributes($data/descendant-or-self::annotation-item,
                                                     xs:QName("id"),
                                                     $id)

        let $anno := functx:add-or-update-attributes($anno,
                                                     xs:QName("modifier"),
                                                     $anno/@creator/string())

        let $anno := functx:remove-attributes($anno, "temp-id")
        let $original-value := annotation:original-value($document-id, $data)
        let $body := functx:add-or-update-attributes($data/descendant-or-self::annotation-item//annotation-body,
                                                     xs:QName("original-value"),
                                                     $original-value)

        let $anno := local:replace-annotation-body($anno, $body)

        let $path := $config:anno-root || '/' || substring($document-id, 1, 3) || '/' || $document-id || '_annotations.xml'
        let $doc:=if(doc($path)) then doc($path) else doc(annotation:setup($document-id))
        let $insert :=  if (annotation:validate-targets($anno))
                        then (update insert $anno into $doc//annotation-list)
                        else (
                            let $x := console:log("Target validation failed.")
                            return ()
                            )

        let $anno := $doc//annotation-item[@id=$id]

        return if($anno) then annotation:status('OK', $anno) else annotation:status('ERR_INSERT')
    else annotation:status('ERR_DOC')
};

declare function annotation:milestone-chunk-text($m1, $m2, $document-id) {
    collection($config:normalized-root || '/' || substring($document-id, 1, 3))/id($document-id)//id($m1) | (collection($config:normalized-root || '/' || substring($document-id, 1, 3))/id($document-id)//id($m1)/following::* intersect collection($config:normalized-root || '/' || substring($document-id, 1, 3))/id($document-id)//id($m2)/preceding::*) | collection($config:normalized-root || '/' || substring($document-id, 1, 3))/id($document-id)//id($m2)
};

declare function annotation:original-value($document-id, $anno) {
    let $target := $anno//target-selector[@type="IdSelector"]/@value/string()
    
    let $original-node :=  
        if($anno//annotation-body/@subtype='join') then
            <seg>{fn:string-join(annotation:milestone-chunk-text($anno//start-selector/@value, $anno//end-selector/@value, $document-id), ' ') || ' '}</seg>
        else 
            collection($config:normalized-root || '/' || substring($document-id, 1, 3))/id($document-id)//id($target)

    return 
            switch ($anno//annotation-body/@subtype)
                case "update.attribute" return 
                    let $q := '$original-node/@' || $anno//annotation-body/attribute/@name || '/string()'
                    return util:eval($q)
                (: update, split, regularization ...       :)
                default return $original-node/string()
};

declare function annotation:status($status as xs:string) {
    annotation:status($status, ())
};

declare function annotation:status($status as xs:string?, $data as node()?) {
    switch ($status)
        case "OK" return (response:set-status-code(200), $data)
        case "ERR_DOC" return (response:set-status-code(400), <error><reason>no document id provided</reason><code>400</code></error>)
        case "ERR_DELETE" return (response:set-status-code(500), <error><reason>delete failed</reason><code>500</code></error>)
        case "ERR_INSERT" return (response:set-status-code(500), <error><reason>couldn't store annotation</reason><code>500</code></error>)
        case "ERR_UPDATE" return (response:set-status-code(500), <error><reason>couldn't update annotation</reason><code>500</code></error>)
        case "ERR_NOT_FOUND" return (response:set-status-code(404), <error><reason>annotation not found</reason><code>404</code></error>)
        case "ERR_FORBIDDEN" return (response:set-status-code(403), <error><reason>{$data/reason}</reason><code>403</code></error>)
        default return (response:set-status-code(500), <error><reason>unknown error</reason><code>500</code></error>)
};

(:~ Set up necessary collection & file to store annotations for a document
 : @param $document-id document's @xml:id
 :)
declare function annotation:setup($document-id as xs:string) {
let $annotations := <annotation-list/>
let $collection_path := $config:anno-root || '/' || substring($document-id, 1, 3)
let $document_name := $document-id || "_annotations.xml"
return
    if(xmldb:collection-available($collection_path))
        then (
            let $status := xmldb:store($collection_path, $document_name, $annotations)
            let $tmp := sm:chgrp(xs:anyURI($collection_path || '/' || $document_name), $config:user-group)
            let $tmp := sm:chmod(xs:anyURI($collection_path || '/' || $document_name), "rwxrwxr--")
(:            let $tmp := sm:chown(xs:anyURI($collection_path || '/' || $document_name), $config:admin-group):)
            return $status

        )
        else (
            let $collection := xmldb:create-collection($config:anno-root, substring($document-id, 1, 3))
            return if ($collection)
                then (
                    let $tmp := sm:chgrp(xs:anyURI($collection_path), $config:user-group)
                    let $tmp := sm:chmod(xs:anyURI($collection_path), "rwxrwxr--")
(:                    let $tmp := sm:chown(xs:anyURI($collection_path), $config:admin-group):)
                    let $status := xmldb:store($collection_path, $document_name, $annotations)
                    let $tmp := sm:chgrp(xs:anyURI($collection_path || '/' || $document_name), $config:user-group)
                    let $tmp := sm:chmod(xs:anyURI($collection_path || '/' || $document_name), "rwxrwxr--")
(:                    let $tmp := sm:chown(xs:anyURI($collection_path || '/' || $document_name), $config:admin-group):)
                    return $status
                )
                else
                    ()
        )
};

declare function annotation:tokenize-params($input) {
    tokenize($input, '\s+')
};

(:origin: imported | earlyPrint:)
(:status: accepted | pending | rejected:)
(:visibility: public | private:)
declare function annotation:create-filter($param, $label) {

    if(count($param)) then "[@" || $label || "=('" || string-join($param, "', '") || "')]" else ()
};

declare function annotation:get-descendants($document-id as xs:string, $version as xs:string?, $fragment-id as xs:string) {
    collection($config:normalized-root || '/' || substring($document-id, 1, 3))//tei:TEI[@xml:id=$document-id][@n=$version]//id($fragment-id)//@xml:id/string()
};

declare function annotation:get-range($document-id as xs:string, $version as xs:string?, $start-id as xs:string?, $end-id as xs:string?) {
    let $intersect :=  collection($config:normalized-root || '/' || substring($document-id, 1, 3))//tei:TEI[@xml:id=$document-id][@n=$version]//id($start-id)/following::*                  intersect
                        collection($config:normalized-root || '/' || substring($document-id, 1, 3))//tei:TEI[@xml:id=$document-id][@n=$version]//id($end-id)/preceding::*
    return $intersect//@xml:id/string()
};


declare function annotation:get-filter-string($generator as xs:string*, $creator as xs:string*, $visibility as xs:string*, $status as xs:string*) {
        annotation:create-filter($generator, 'generator') ||
        annotation:create-filter($creator, 'creator') ||
        annotation:create-filter($visibility, 'visibility') ||
        annotation:create-filter($status, 'status')

};

declare function annotation:get-annotations($document-id as xs:string, $version as xs:string?, $fragment as element(), $filters as xs:string*) {
    (: First find all annotations matching the given filters :)
    let $collection := doc($config:anno-root || "/" || substring($document-id, 1, 3) || "/" || $document-id || "_annotations.xml")
    let $v := if($version) then '[annotation-target/@version="' || $version || '"]' else ()
    let $doc := if($document-id) then '[annotation-target/@source="' || $document-id || '"]' else ()
    let $queries := for-each($filters, function ($filter as xs:string) {
        '$collection//annotation-item' || $v || $doc || $filter
    })
    let $candidates := util:eval(string-join($queries, '|'))
    (: then find annotations targeting the xml:ids in $fragment :)
    let $byId :=
        for $id in $fragment//@xml:id
        return
            (: both single- and multiple-target annotations :)
            $collection//annotation-item[annotation-target/target-selector/@value = $id]
            | $collection//annotation-item[annotation-target/target-selector/start-selector/@value = $id]

    return
        (: now merge the two sets :)
        $candidates intersect $byId
};


declare function annotation:get-annotations($document-id as xs:string, $version as xs:string?, $fragment-id as xs:string?, $descendants as xs:string*, $filters as xs:string*) as node()* {
    let $v := if($version) then '[@version="' || $version || '"]' else ()
    let $doc := if($document-id) then '[@source="' || $document-id || '"]' else ()

    let $fragment := if($fragment-id or exists($descendants)) then '[target-selector/@value=$descendants]' else ()

    let $prolog :=
        if (empty($descendants)) then
            ('let $descendants := annotation:get-descendants("' || $document-id || '","' || $version || '","' || $fragment-id || '") return ')
        else
            ()
    let $query :=
        $prolog || 'collection("' || $config:anno-root || "/" || substring($document-id, 1, 3) || '/' || $document-id || '_annotations.xml' || '")//annotation-target' || $v || $doc || $fragment
        || '/parent::annotation-item' || $filters

(:    let $c := console:log($query):)
    let $annotations := util:eval($query)
    return <annotation-list id="annotation-list">{$annotations}</annotation-list>
};

declare function annotation:get-annotations-for-range($document-id as xs:string, $version as xs:string?, $start-id as xs:string?, $end-id as xs:string?, $filters as xs:string*) as node()* {

    let $v := if($version) then '[@version="' || $version || '"]' else ()
    let $doc := if($document-id) then '[@source="' || $document-id || '"]' else ()

    let $fragment := if($start-id) then '[target-selector/@value=$contained]' else ()

    let $query := '

    let $contained := annotation:get-range("' || $document-id || '","' || $version || '","' || $start-id || '","' || $end-id || '")
    return
    collection("' || $config:anno-root || "/" || substring($document-id, 1, 3) || $document-id || '_annotations.xml' || '")//annotation-target' || $v || $doc || $fragment
                || '/parent::annotation-item' || $filters
(:                let $c := console:log($query):)
    let $annotations := util:eval($query)
    return <annotation-list id="annotation-list">{$annotations}</annotation-list>
};
