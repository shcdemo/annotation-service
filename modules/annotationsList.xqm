xquery version "3.0";
(:~ 
 : Service for providing filtered annotations list
 : 
 : @param $source @xml:id of the document
 : @param $version document version number
 : @param $fragment @xml:id of the document fragment
 : @param $filter filter criteria
 : @return list of annotations
 :)

declare namespace tei = "http://www.tei-c.org/ns/1.0";
import module namespace s2i="http://existsolutions.com/annotation-service/standoff2inline" at "standoff2inline.xqm";
import module namespace annotation="http://existsolutions.com/annotation-service/annotation" at "annotation.xql";
import module namespace config="http://existsolutions.com/annotation-service/config" at "config.xqm";

let $document-id := request:get-parameter('source', 'Ad9116c1e-c323-431a-a880-b56c08beafcc')
let $version := request:get-parameter('version', '1')
let $fragment-id := request:get-parameter('fragment', 'A11909_01-e100050')
let $generator := annotation:tokenize-params(request:get-parameter('generator', 'import'))
let $creator := annotation:tokenize-params(request:get-parameter('creator', 'earlyPrint daemon'))
let $visibility := annotation:tokenize-params(request:get-parameter('visibility', 'public private'))
let $status := annotation:tokenize-params(request:get-parameter('status', ''))

let $filters := annotation:get-filter-string($generator, $creator, $visibility, $status)
    
return annotation:get-annotations($document-id, $version, $fragment-id, $filters)