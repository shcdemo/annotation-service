xquery version "3.0";

module namespace normalization="http://existsolutions.com/annotation-service/normalization";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";

declare namespace tei = "http://www.tei-c.org/ns/1.0";

declare function normalization:normalize($input as item()*) as item()* {
for $node in $input
   return 
      typeswitch($node)
        (: add @n to the TEI element :)
        case element(tei:TEI)
            return
                    element {node-name($node)} {
                        for $att in $node/@*[name() ne 'n']
                           return $att 
                        ,
                        attribute n {1}
                        ,
                        for $child in $node
                            return normalization:normalize($child/node())
                    }
        (: strip choices from inside of w elements :)
        case element(tei:choice)
            return
                if($node/parent::tei:w) 
                then ($node/tei:orig/string())
                else 
                    element {node-name($node)} {
                        for $att in $node/@*
                           return $att
                        ,
                        for $child in $node
                            return normalization:normalize($child/node())
                    }
        case element()
           return
              element {node-name($node)} {
                for $att in $node/@*[name() ne 'xml:id']
                   return $att
                ,
                if($node/@xml:id) then attribute xml:id {translate($node/@xml:id, '.', '-')} else ()
                ,
                for $child in $node
                   return normalization:normalize($child/node())
              }
        (: all the rest pass it through :)
        default 
            return $node
};

declare function normalization:cleanup-client-data($input as item()*) as item()* {
for $node in $input
   return 
      typeswitch($node)
        case document-node()
            return
                for $child in $node
                   return normalization:cleanup-client-data($child/node())
        case element()
           return
              element {node-name($node)} {
                (:skip @class attribute coming from client :)
                for $att in $node/@*[name() ne 'class']
                   return $att
                ,
                for $child in $node
                   return normalization:cleanup-client-data($child/node())
              }
        (: all the rest pass it through :)
        default 
            return $node
};