xquery version "3.0";

module namespace i2s="http://existsolutions.com/annotation-service/inline2standoff";

import module namespace config="http://existsolutions.com/annotation-service/config" at "config.xqm";
declare namespace tei = "http://www.tei-c.org/ns/1.0";

(:~
 : extracting regularizations only for the time being
 : in general should deal with all types of markup that we provide custom components for
 :)
 
declare function i2s:extract($node as node()) {
    i2s:extractRegularizations($node)
};

    (: might be good to keep TEI xml:id for annotations originating from inline TEI  :)
    (: TODO assign sensible @version numbers :)
declare function i2s:extractRegularizations($node as node()) {
    let $source := root($node)/tei:TEI/@xml:id
    for $i in $node//tei:w[@reg]
    let $id:=$i/@xml:id
    return 
    <annotation-item>
        {attribute id { if($id) then $id else 'A' || util:uuid()}}
        {attribute creator {"earlyPrint"}}
        {attribute visibility {"public"}}
        {attribute generator {"import"}}
        {attribute status {"accepted"}}
        {attribute created {"2016-01-01"}}
         <annotation-body type="TEI" subtype="regularization" format="text/xml">
           {attribute original-value {$i/string()}}
           <orig>{$i/string()}</orig>
            <reg>{$i/@reg/string()}</reg>
        </annotation-body>
        <annotation-target>
            {attribute source {$source}}
            {attribute version {1}}
            <target-selector type="IdSelector">
                {attribute value {$id}}
            </target-selector>
        </annotation-target>
    </annotation-item>
};