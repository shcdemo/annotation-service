xquery version "3.0";

(: The following external variables are set by the repo:deploy function :)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;

sm:chmod(xs:anyURI($target || "/modules/create.xqm"), "rwxr-Sr-x"),
sm:chgrp(xs:anyURI($target || "/modules/create.xqm"), "dba"),
sm:chmod(xs:anyURI($target || "/modules/review.xqm"), "rwxr-Sr-x"),
sm:chgrp(xs:anyURI($target || "/modules/review.xqm"), "dba"),
sm:chmod(xs:anyURI($target || "/modules/update.xqm"), "rwxr-Sr-x"),
sm:chgrp(xs:anyURI($target || "/modules/update.xqm"), "dba"),
sm:chmod(xs:anyURI($target || "/modules/delete.xqm"), "rwxr-Sr-x"),
sm:chgrp(xs:anyURI($target || "/modules/delete.xqm"), "dba")
