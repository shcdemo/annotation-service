# Back End App for EarlyPrint Project

Responsible for:
1. Expose annotations to the client(s) via an API
2. Merge annotations back into fragments of TEI source files for web display

## Organization of the Collections

### Texts

`/db/apps/EarlyPrintTexts/texts` by default but configurable in `modules/config.xqm;$config:data-root` and `modules/config.xqm;$config:texts-root`

### Annotations

`/db/apps/EarlyPrintAnnotations/annotations` by default but configurable in `modules/config.xqm;$config:anno-root`

## Import

There is latent, incomplete, and out-of-date support for importing arbitrary TEI documents, storing them
in an import collection, and extracting annotations for storage in a separate collection.  At the time this
feature was abandoned, it could only create "annotations" for regular spellings, which was a grossly
inefficient way to store regular spellings that is no longer in use.  We are also using a simple and strict
TEI schema with plans to load 60,000 EEBO-TCP texts with a consistent encoding standard.  There is no use
case of any interest for supporting user uploads of TEI files that use different schemas or are not part
of this corpus.


## API

> xquery module to store annotation based on the data sent from front-end
> It should follow REST standards or best-practises.

* merge
* list
* range
* create
* read
* update
* delete

### Errors
Especially **error-handling** should use the http status codes for rough categorisation of the problem along with a human readable error message and a error code for client side handling of the problem.

failure: Status 401, 403, 500...
```xml
<error><reason>something human readable</reason><code>12345</code></error>
```
### Merge

`GET /annotation/merge/{document-id}/{fragment-id}`

e.g. https://admin.exist-db.org:44443/exist/apps/annotation-service/annotation/merge/Ad9116c1e-c323-431a-a880-b56c08beafcc/A11909_01-e100040

calls `modules/merge.xqm` to retrieve a *regular* TEI document (or fragment) with all standoff markup inlined

fragment-id can be omitted to retrieve full document

further parameters may be passed to filter annotations

**see far down for detailed discussion of merge algorithm**

### List

`GET /annotation/list/{document-id}/{fragment-id}`

e.g. https://admin.exist-db.org:44443/exist/apps/annotation-service/annotation/list/Ad9116c1e-c323-431a-a880-b56c08beafcc/A11909_01-e100040

returns a list of annotations relevant to the specified document or document fragment

fragment-id can be omitted to retrieve annotations for full document

further parameters may be passed to filter annotations

### Range

`GET /annotation/range/{document-id}/{start-id}/{end-id}`

e.g. https://admin.exist-db.org:44443/exist/apps/annotation-service/annotation/range/A01353/A01353-000030/A01353-000890

returns a list of annotations relevant to the document fragment between `start-id` and `end-id`

further parameters may be passed to filter annotations

### Create

`POST /annotation`

currently `POST /annotation/create`

e.g. https://admin.exist-db.org:44443/exist/apps/annotation-service/annotation/create

The client can provide a temporary id as the `temp-id` attribute on the `annotation-client` element.
The server must include this temporary ID in a response. (This will be used to replace/update temporary elements created on the client-side).

success:
Status 200

failure:
no data: 400
unauthorized: 403

```xml
<annotation-item id='1' temp-id='xxx' ...></annotation-item>
```
// whatever is sent here will replace the annotation on the client

### Read

`GET /annotations/{annotation-id}`

currently `GET /annotation/get/{annotation-id}`

e.g. http://localhost:8080/exist/apps/annotation-service/annotation/get/898b1b1d-8b2d-442d-9002-4ff382a5a57c

Status 200, 404
```xml
<annotation-item id='1'></annotation-item>
```
### Update

`POST /annotations/{annotation-id}`
(maybe PUT but only if it is not well enough supported)

success:
Status 200, 403, 404, 500
```xml
<annotation-item id='1'>...</annotation-item>
```
// whatever is sent here will replace the annotation on the client

### Delete

`GET /annotations/delete/{annotation-id}`

currently `GET /annotation/delete/{annotation-id}`

e.g. http://localhost:8080/exist/apps/annotation-service/annotation/delete/{annotation-id}

Status 200, 404, 403, 500

## annotation characteristics

* type (annotation-item/annotation-body/@type)
* subtype (annotation-item/annotation-body/@subtype)
* creator (annotation-item/@creator)
* modifier (annotation-item/@modifier)
* status (annotation-item/@status)
* visibility (annotation-item/@visibility)
* origin (annotation-item/@generator)


```XML

<annotation-item modifier="skye@example.com" id="Ab76556c0-9079-4a12-9428-d240e868ed08" generator="earlyPrint" status="pending" visibility="public" reason="" creator="skye@example.com" created="2019-07-10T18:13:33.043Z" modified="2019-07-10T18:13:33.043Z" generated="2019-07-10T18:13:33.043Z" ticket="s-1562782413042">
    <annotation-body original-value="●erui" subtype="update" format="text/xml" type="TEI">
        <w>servi</w>
    </annotation-body>
    <annotation-target source="A02644" version="">
        <target-selector type="IdSelector" value="A02644-006-a-1540"/>
    </annotation-target>
</annotation-item>
     
```

### subtypes

* update (single word correction of content)
* update.attribute (modify an attribute of a word)
* join (join multiple words together)
* split (split word in two)

N.B. We use subtypes since the general type of all our annotations is "TEI."

### creator

The username of the person who created the annotation.

### modifier

The username of the person who last modified the annotation, such as an editor who approved it.

### origin

* currently only earlyPrint is used (created with earlyPrint annotation module)

### status

* accepted (reviewed and approved)
* pending (pending review)
* rejected (reviewed and rejected)

### visibility

* public 
* private (visible for the creator only)

## Annotation format examples

### single target (updating the standardized spelling via the "reg" attribute)
```XML
<annotation-item modifier="casey@example.com" id="A0669eea8-ff43-4965-a80e-f285a9fbb909" generator="earlyPrint" status="pending" visibility="public" reason="" creator="casey@example.com" created="2019-07-24T17:45:08.048Z" modified="2019-07-24T17:45:08.048Z" generated="2019-07-24T17:45:08.048Z" ticket="s-1563990308038">
    <annotation-body original-value="" subtype="update.attribute" format="text/xml" type="TEI">
        <attribute name="reg">would</attribute>
    </annotation-body>
    <annotation-target source="A59427" version="">
        <target-selector type="IdSelector" value="A59427-033-a-2000"/>
    </annotation-target>
</annotation-item>
```

### range target (e.g. word join)
```XML
<annotation-item modifier="casey@example.com" id="A701a1a07-0e12-4a0e-894d-3089b16cada2" generator="earlyPrint" status="approved" visibility="public" reason="" creator="ccasey@example.com" created="2019-07-24T18:02:23.754Z" modified="2019-07-25T08:24:17.237Z" generated="2019-07-24T18:02:23.754Z" ticket="s-1563991343753">
    <annotation-body original-value="〈◊〉" subtype="join" format="text/xml" type="TEI">
        <w>beyond</w>
    </annotation-body>
    <annotation-target source="A59427" version="">
        <target-selector type="RangeSelector">
            <start-selector type="IdSelector" value="A59427-037-b-3360"/>
            <end-selector type="IdSelector" value="A59427-037-b-3370"/>
        </target-selector>
    </annotation-target>
</annotation-item>
```

## Document life cycle

![earlyPrint](/uploads/6503f9c40993076bb6d1e911aec5b3e5/earlyPrint.png)

## Merge

Merge algorithm has to:
- be stable (merge results should be the same irrespective of the way the annotations are stored and as far as possible
of the order they are created/accepted in; what follows, there 
    - must follow some well-specified policy for nesting various types of annotations
    - source documents need to undergo a normalization process
- perform well enough to process the volume of material we can expect
- deal with conflicts.

Potential conflicts:
- annotations are non-mergeable as this would lead to element overlap
- annotations refer to something that was previously deleted (potentially preventable by performing deletes last)

Some arbitrary rules to keep us sane:
- `<w>` and `<pc>` elements are considered atomic for the time being so text nodes only, all have @xml:ids
- whitespace wrapped with `<c>` can be probably ignored
- choice elements can only contain single `<w>` element
- person, place and app annotations may target consecutive runs of words but without crossing higher-level element boundaries (such as `<p>, <l>, <div>`)
- nesting guideline: app person place choice w (see http://admin.exist-db.org:9090/the-early-modern-lab/annotation-service/issues/1#note_16 for examples)

## Terminology addendum

* __annotation__: actually a bad term to be used by us because in the strict technical sense refers to statements made
 about a text, which are never part of the text. An annotation does not modify the 
 source document. It only refers to the document or some object inside it. 
 We will have real annotations in EML when we implement general user notes, but 
 it's only one use case, whereas most other use cases can not be named annotations 
 in the strict sense, but are rather stand-off markup.
* __stand-off markup__: results from externalizing markup in such a way that the
original TEI document can be losslessly reconstructed by combining the resulting base text with 
the stand-off markup.
* __input text__: the unmodified TEI we received, containing arbitrary markup as allowed by the TEI schema
* __reference text__: a normalized form of the input text with some basic rules applied, e.g. concerning the nesting of elements. The reference text serves as input to the annotation process, and an export will always result in a new reference text, which can again be used as input. The rules imposed must ensure a roundtrip will work properly.
* __base text__: contains only structural markup, but no text-level inline markup, 
which is all externalized into stand-off markup. Exceptions defined below.
* __transcription__: the best possible reproduction of the original text resulting from the
editorial process, usually extended with a commentary. Creating a transcription always involves interpretation and 
editorial decisions. The general goal of TEI is to make all interpretations and decisions explicit,
so they can be challenged by other scholars. The transcription is the final outcome of the whole process.

For the EEBO corpus, we decide to keep all linguistic markup in the __base text__
and not externalize it into stand-off markup. Without this decision, we would have
to deal with millions of stand-off elements up front, which would complicate things
to the extreme. While EEBO contains linguistic markup, we have to be aware that most other
text collections will not, and the overall goal of our project is to create a toolbox which
can be applied to *any* TEI following certain conventions.

For milestone 1 we also distinguish two different types of editorial acts, where type 1 is rather basic and type 2 includes everything else (later milestones will have to expand on 2):

1. __correction__ of obvious __defects__ in the transcription resulting e.g. from bad OCR. 
The target for these corrections will mainly be the w and pc elements. Contrary to (2) below, the editorial decision to accept a correction or not will not be reflected in the 
TEI, but is only visible by looking at the change log.
2. any other type of __editorial commentary__, which e.g.
    * expands on features manifest in the original text (e.g. a correction made by 
    the author, or a gap or damage in the facsimile)
    * statements about the text which involve interpretation and could be challenged
    * any hints to the reader, e.g. by giving a regularized form for a word given 
    in the original text (this always involves interpretation!). The choice[reg|orig] elements 
    fall into this category.
    * semantic markup to help the reader identify e.g. persons, places, common terms...